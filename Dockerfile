FROM registry.gitlab.com/rynojvr/n2n-docker:latest as repo

FROM debian:stretch
COPY --from=repo /usr/sbin/edge /usr/sbin/
COPY edge.conf.sample /n2n/edge.conf

ENTRYPOINT ["/usr/sbin/edge", "/n2n/edge.conf"]
